from rest_framework import serializers
from TestApi.models import ArticlePost

class ArticleSerializer(serializers.Serializer):
    class Meta:
        model = ArticlePost
        fields = ['id', 'title', 'body']
        