from django.db import models

# Create your models here.

class ArticlePost(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255, blank=True, default='')
    body = models.TextField()
    
    class Meta:
        ordering = ['created_at']
      